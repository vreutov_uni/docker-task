import os
import pandas as pd
from sqlalchemy import create_engine

from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report

print("Connecting to database...")
db_host = os.environ['DB_HOST']
db_port = os.environ['DB_PORT']
db_user = os.environ['DB_USER']
db_pass = os.environ['DB_PASS']

engine = create_engine(
    f'postgresql://{db_user}:{db_pass}@{db_host}:{db_port}')

print("Connected to database, reading data...")
df = pd.read_sql_query('select * from "knn_data"', con=engine)

print("Preparing data...")
X = df.drop('attr', axis=1)
y = df['attr']

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=42)

print("Classification using KNeighborsClassifier with k=5:")
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)
y_predict = knn.predict(X_test)

print(classification_report(y_test, y_predict))

print("Classification using RandomForestClassifier:")
rfc = RandomForestClassifier(n_estimators=100)
rfc.fit(X_train, y_train)
y_predict = rfc.predict(X_test)

print(classification_report(y_test, y_predict))

