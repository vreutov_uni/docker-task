CREATE TABLE knn_data
AS
SELECT 
  x.id AS x,
  y.id AS y,
  CASE WHEN (random() > 0.05) THEN
         CASE WHEN x.id + y.id < 30 THEN 'A'
           WHEN x.id + y.id < 65 THEN 'B'
           ELSE 'C'
         END
       WHEN random() > 0.66 THEN 'A'
       WHEN random() > 0.66 THEN 'B'
       ELSE 'C'
   END AS attr         
FROM generate_series(1, 100) AS x(id) CROSS JOIN generate_series(1, 100) AS y(id)
WHERE random() > 0.3;

