# Лабораторная работа по Docker
## Задание
1. Создать образ Postgres с готовой БД
1. Создать образ Python с библиотекой машинного обучения (1-2 метода) + скрипт,
который в качестве датасета для алгоритма машинного обучения использует
таблицу/таблицы БД из первого образа и выдает результат машинного обучения на
экран
1. Сделать compose file, включающий в себя описание 2-х сервисов на основе созданных
образов, запустить сервисы
1. Запустить скрипт из второго образа для демонстрации результатов

## Выполнение задания
Для решения были взяты данные из задания по работе в среде Databricks (классификация двумерных точек)

В качестве библиотеки машинного обучения был выбран пакет [scikit-learn](https://scikit-learn.org/stable/), из которого для классификации были использованы алгоримты k ближайших соседей и случайный лес

## Инструкция по запуску
### Запустить контейнеры
```bash
docker-compose up -d
```

### Зайти на контейнер с python
```bash
docker-compose exec scripts sh
```

### Запустить скрипт
```bash
python3 main.py
```

### Результат
```
Connecting to database...
Connected to database, reading data...
Preparing data...
Classification using KNeighborsClassifier with k=5:
              precision    recall  f1-score   support

           A       0.94      0.70      0.80       109
           B       0.95      0.95      0.95       354
           C       0.97      0.99      0.98      1614

   micro avg       0.97      0.97      0.97      2077
   macro avg       0.96      0.88      0.91      2077
weighted avg       0.97      0.97      0.97      2077

Classification using RandomForestClassifier:
              precision    recall  f1-score   support

           A       0.90      0.69      0.78       109
           B       0.93      0.92      0.93       354
           C       0.97      0.99      0.98      1614

   micro avg       0.96      0.96      0.96      2077
   macro avg       0.93      0.87      0.90      2077
weighted avg       0.96      0.96      0.96      2077
```